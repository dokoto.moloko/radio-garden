#!/bin/bash

id=$(radio-garden -l|fzf|cut -d " " -f 1)
if [ ! -z "$id" ]
then
  radio-garden --radio-id "$id" --delete-radio
  echo "$id removed radio"
fi
