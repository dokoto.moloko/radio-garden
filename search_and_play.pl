#!/usr/bin/perl

$id= `radio-garden -s $ARGV[0]|fzf|cut -d " " -f 1`;
# chomp remove \n newline
chomp($id);
if ( length($id) != 0 ) {
  $url = `radio-garden --radio-id $id --export-url`;
  chomp($url);
  exec "mpv $url --no-video";
}
