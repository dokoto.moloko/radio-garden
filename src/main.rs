use std::process;
mod api;
mod utils;

type ResultTokio<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

#[tokio::main]
async fn main() -> ResultTokio<()> {
    let args_matches = utils::parse_args();
    if args_matches.is_present("search") {
        let search = args_matches.value_of("search").unwrap();
        if let Err(e) = api::search_radios(search).await {
            print!("{}", e);
            process::exit(-1);
        } 
    }
    else if args_matches.is_present("id") && args_matches.is_present("export") {
        let id = args_matches.value_of("id").unwrap();
        // https://radio.garden/api/ara/content/listen/PQTGIA5d/channel.mp3?1644262134744
        println!("{}/ara/content/listen/{}/channel.mp3", api::RADIO_GARDEN_HOST, id);
    }
    else if args_matches.is_present("id") && args_matches.is_present("save") {
        let id = args_matches.value_of("id").unwrap();
        if let Err(e) = api::save_radio(id).await {
            print!("{}", e);
            process::exit(-1);
        }
    }
    else if args_matches.is_present("list") {
        let filter = args_matches.value_of("filter");
        if let Err(e) = api::list_radios(&filter) {
            print!("{}", e);
            process::exit(-1);
        }
    }
    else if args_matches.is_present("id") && args_matches.is_present("delete") {
        let id = args_matches.value_of("id").unwrap();
        if let Err(e) = api::delete_radio(id) {
            print!("{}", e);
            process::exit(-1);
        }
    }
    Ok(())
}
