use clap::{ Arg, App, ArgMatches };

pub fn parse_args<'a>() -> ArgMatches<'a> {
    App::new("rg")
        .version("0.1.0")
        .about("Listen RagioGarden on terminal")
        .author("Dokoto <trabajo@dkt-mlk.es>")
        .arg(Arg::with_name("search")
          .help("Search readios by name")
          .short("s")
          .long("search-new-radios")
          .takes_value(true),
        )
        .arg(Arg::with_name("id")
          .help("Radio ID to make actions over it")
          .short("id")
          .long("radio-id")
          .takes_value(true),
        )
        .arg(Arg::with_name("filter")
          .help("Filter radio list by name")
          .long_help("Filter radios list by name mixed with --search and --list-my-radio")
          .short("f")
          .long("filter-radio-list")
          .takes_value(true),
        )
        .arg(Arg::with_name("list")
          .help("List saved radios")
          .short("l")
          .long("list-my-radios")
        )
        .arg(Arg::with_name("export")
            .help("Export full radio url for using with a player like mpv")
            .long_help("Comnined with -id export an url. $> radio-garden -id 232432 --export-url")
            .long("export-url"),
        )
        .arg(Arg::with_name("save")
            .help("Save radio as favorite")
            .long_help("Comnined with -id save radio as favorite. $> radio-garden -id 232432 --save")
            .long("save-radio"),
        )
        .arg(Arg::with_name("delete")
            .help("Delete radio as favorite")
            .long_help("Comnined with -id remove a radio. $> radio-garden -id 232432 --delete")
            .long("delete-radio"),
        )
        .get_matches()
}
