use std::fs;
use std::error;
use std::io::{ BufWriter, Write, BufReader };
use serde::{ Deserialize, Serialize };
use dirs::config_dir;
use regex::Regex;
use std::path::PathBuf;

pub const RADIO_GARDEN_HOST: &str = "https://radio.garden/api";
const STORAGE_PATH: &str = "radio-garden";

#[derive(Deserialize, Debug)]
struct Source {
    code: String,
    #[serde(default)]
    subtitle: String,
    title: String,
    url: String,
}

#[derive(Deserialize, Debug)]
struct Hit {
    #[serde(rename = "_source")]
    source: Source,
}

#[derive(Deserialize, Debug)]
struct Hits {
    hits: Vec<Hit>,
}

#[derive(Deserialize, Debug)]
struct RadiosResp {
    hits: Hits,
}

#[derive(Debug)]
pub struct Radio {
    id: String,
    country: String,
    place: String,
    name: String,
}

#[derive(Deserialize, Serialize, Debug)]
struct IdTitle {
    id: String,
    title: String,
}

#[derive(Deserialize, Serialize, Debug)]
struct RadioData {
    title: String,
    id: String,
    place: IdTitle,
    country: IdTitle,
}

#[derive(Deserialize, Debug)]
struct RadioResp {
    data: RadioData,
}

fn get_channel_id(channel: &str) -> Option<String> {
    match Regex::new(r#"/[listen|visit]/*.+/(\S+)"#) {
        Ok(rx) => {
            match rx.captures(channel) {
                Some(cap) => {
                    if cap.len() < 2 { return None };
                    Some(String::from(&cap[1]))
                }
                None => None,
            }
        }
        Err(_) => None,
    }
}

pub async fn search_radios(name: &str) -> Result<(), Box<dyn error::Error>> {
    let url = format!("{}/search?q={}", RADIO_GARDEN_HOST, name);
    let client = reqwest::Client::new();
    let response = client
      .get(url)
      .header("content-type", "application/json")
      .header("accept", "application/json")
      .send()
      .await?;
    let body = response.json::<RadiosResp>().await?;
    for radio in &body.hits.hits {
        let id = get_channel_id(&radio.source.url).unwrap();
        println!("{0: <10} {1: <50} [{2: <2}] {3: <50}",
                 id, radio.source.title, radio.source.code, radio.source.subtitle);
    }
    Ok(())
}

pub fn list_radios(filter: &Option<&str>) -> Result<(), Box<dyn error::Error>> {
    let radios = read_radios_from_file()?;
    radios
        .iter()
        .filter(|r| {
            if filter.is_some() {
                let filter_val = filter.unwrap();
                r.title.to_lowercase().contains(&filter_val.to_lowercase())
            } else {
                true
            }
        })
        .for_each(|radio| { 
            println!("{0: <10} {1: <50} [{2: <2}] {3: <50}",
                     radio.id, radio.title, radio.country.title, radio.place.title);
        });
        Ok(())
}

fn get_config_file () -> Result<(PathBuf, PathBuf), Box<dyn error::Error>> {
    let mut my_config_dir = config_dir().ok_or("No config dir found")?;
    my_config_dir.push(STORAGE_PATH);
    let mut my_config_file = PathBuf::from(&my_config_dir);
    my_config_file.push("radios.json");
    Ok((my_config_dir, my_config_file))
}

fn read_radios_from_file() -> Result<Vec<RadioData>, Box<dyn error::Error>> {
    let err_no_radios = || return Err("Radios not found".to_string());
    let (_, my_config_file) = get_config_file()?;
    if my_config_file.exists() {
        let radios_file = fs::File::options()
            .read(true)
            .open(&my_config_file)?;
        let radio_file_reader = BufReader::new(&radios_file);
        let radios: Vec<RadioData> = serde_json::from_reader(radio_file_reader)?;
        Ok(radios)
    } else {
        err_no_radios()?
    }
}

fn write_radios_on_file(radios: &Vec<RadioData>) -> Result<(), Box<dyn error::Error>> {
    let (_, my_config_file) = get_config_file()?;
    let radios_file = fs::File::options()
        .write(true)
        .truncate(true)
        .open(&my_config_file)?;
    let mut radios_file_writer = BufWriter::new(&radios_file);
    serde_json::to_writer(&mut radios_file_writer, &radios)?;
    radios_file_writer.flush()?;
    Ok(())
}

fn create_radios_on_file(radios: &Vec<RadioData>) -> Result<(), Box<dyn error::Error>> {
    let (_, my_config_file) = get_config_file()?;
    let radios_file = fs::File::options()
        .write(true)
        .create(true)
        .open(&my_config_file)?;
    let mut radios_file_writer = BufWriter::new(&radios_file);
    serde_json::to_writer(&mut radios_file_writer, &radios)?;
    radios_file_writer.flush()?;
    Ok(())
}

pub fn delete_radio(id: &str) -> Result<(), Box<dyn error::Error>> {
    let err_no_radio = || return Err("Radio not found".to_string());
    let mut radios = read_radios_from_file()?; 
    let index_opt = radios.iter().position(|radio| radio.id == id);
    if let Some(index) = index_opt {
        radios.remove(index);
        write_radios_on_file(&radios)?;
    } else {
        err_no_radio()?
    }
    Ok(())
}

// https://radio.garden/api/ara/content/channel/F3vJ1NYj
async fn req_radio_detail(id: &str) -> Result<reqwest::Response, reqwest::Error> {
    let url = format!("{}/ara/content/channel/{}", RADIO_GARDEN_HOST, id);
    let client = reqwest::Client::new();
    client
      .get(url)
      .header("content-type", "application/json")
      .header("accept", "application/json")
      .send()
      .await
}

pub async fn save_radio(id: &str) -> Result<(), Box<dyn error::Error>> {
    let response = req_radio_detail(id).await?;
    let (my_config_dir, my_config_file) = get_config_file()?;
    if !my_config_dir.exists() {
        fs::create_dir(&my_config_dir)?;
    } else if my_config_dir.exists() && !my_config_file.exists() {
        let radio = response.json::<RadioResp>().await?;
        let radios = vec![radio.data];
        create_radios_on_file(&radios)?;
    } else if my_config_file.exists() {
        let radio = response.json::<RadioResp>().await?;
        let mut radios = read_radios_from_file()?;
        if radios.iter().find(|r| r.id == radio.data.id).is_none() {
            radios.push(radio.data);
        }
        write_radios_on_file(&radios)?;
    }
    Ok(())
}
