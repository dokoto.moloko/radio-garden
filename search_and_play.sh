#!/bin/bash

id=$(radio-garden -s "$1"|fzf|cut -d " " -f 1)
if [ ! -z "$id" ]
then
  url=$(radio-garden --radio-id "$id" --export-url)
  mpv "$url" --no-video
fi
