#!/bin/bash

id=$(radio-garden -s "$1"|fzf|cut -d " " -f 1)
if [ ! -z "$id" ]
then
  radio-garden --radio-id "$id" --save-radio
  echo "$id radio saved"
fi
